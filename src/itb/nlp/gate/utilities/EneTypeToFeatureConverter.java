/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package itb.nlp.gate.utilities;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.creole.AbstractLanguageAnalyser;
import gate.creole.ExecutionException;
import gate.util.InvalidOffsetException;
import gate.util.OffsetComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asatrya
 */
public class EneTypeToFeatureConverter extends AbstractLanguageAnalyser implements ProcessingResource {
    String inputASName, outputASName, destType, featureLabel;
    Set<String> sourceTypes;
    Boolean keepSourceAnnotations;

    public String getinputASName() {
        return inputASName;
    }

    public void setinputASName(String inputASName) {
        this.inputASName = inputASName;
    }

    public String getoutputASName() {
        return outputASName;
    }

    public void setoutputASName(String outputASName) {
        this.outputASName = outputASName;
    }

    public Set<String> getsourceTypes() {
        return sourceTypes;
    }

    public void setsourceTypes(Set<String> annTypes) {
        this.sourceTypes = annTypes;
    }

    public String getdestType() {
        return destType;
    }

    public void setdestType(String destType) {
        this.destType = destType;
    }

    public String getfeatureLabel() {
        return featureLabel;
    }

    public void setfeatureLabel(String featureLabel) {
        this.featureLabel = featureLabel;
    }

    public Boolean getkeepSourceAnnotations() {
        return keepSourceAnnotations;
    }

    public void setkeepSourceAnnotations(Boolean keepSourceAnnotations) {
        this.keepSourceAnnotations = keepSourceAnnotations;
    }

    @Override
    public void execute() throws ExecutionException {
        Document doc = getDocument();

        AnnotationSet inputAnnSet = (inputASName == null || inputASName.length() == 0)
                ? doc.getAnnotations()
                : doc.getAnnotations(inputASName);

        AnnotationSet outputAnnSet = (outputASName == null || outputASName.length() == 0)
                ? doc.getAnnotations()
                : doc.getAnnotations(outputASName);

        //Get source annotation types
        List<Annotation> sourceAnnList = new ArrayList<Annotation>(inputAnnSet.get(getsourceTypes()));
        Collections.sort(sourceAnnList, new OffsetComparator());

        //Iterate source annotation types
        ListIterator<Annotation> sourceAnnIter = sourceAnnList.listIterator();
        while(sourceAnnIter.hasNext()){
            try {
                Annotation tSourceAnn = sourceAnnIter.next();
                String tSourceClass = tSourceAnn.getFeatures().get(getfeatureLabel()).toString();
                String[] classes = tSourceClass.split("_");
                
                // Create new annotaions and feature
                FeatureMap newFeatures = Factory.newFeatureMap();
                int lastIndex = 0;
                String classStr = "";

                // Put class-1
                if(classes.length >= 1){
                    lastIndex = 0;
                    classStr += classes[lastIndex];
                }
                newFeatures.put("class-1", classStr);

                // Put class-2
                if(classes.length >= 2){
                    lastIndex = 1;
                    classStr += "_" + classes[lastIndex];
                }
                newFeatures.put("class-2", classStr);

                // Put class-3
                if(classes.length >= 3){
                    lastIndex = 2;
                    classStr += "_" + classes[lastIndex];
                }
                newFeatures.put("class-3", classStr);

                // Write feature
                outputAnnSet.add(tSourceAnn.getStartNode().getOffset(), tSourceAnn.getEndNode().getOffset(), getdestType(), newFeatures);
            } catch (InvalidOffsetException ex) {
                Logger.getLogger(AnnTypeToFeatureConverter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Delete source annotations
        if(getkeepSourceAnnotations() == false){
            inputAnnSet.removeAll(sourceAnnList);
        }
    }
}
