/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package itb.nlp.gate.sandbox;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Factory;
import gate.ProcessingResource;
import gate.creole.AbstractLanguageAnalyser;
import gate.creole.ExecutionException;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * @author asatrya
 */
public class JapeSandbox extends AbstractLanguageAnalyser implements ProcessingResource {
    AnnotationSet inputAS, outputAS;
    Bindings bindings = new Bindings();

    class Bindings{

        public Bindings() {
        }

        public AnnotationSet get(String string){
            return (gate.AnnotationSet)new Object();
        }

    }

    @Override
    public void execute() throws ExecutionException {
        //remove old annotations
        gate.AnnotationSet leftAS = (gate.AnnotationSet)bindings.get("left");
        outputAS.removeAll(leftAS);

        //get the tokens text
        java.util.ArrayList tokens = new java.util.ArrayList(leftAS);
        Collections.sort(tokens, new gate.util.OffsetComparator());
        String text = "";
        int length = 0;
        Iterator tokIter = tokens.iterator();
        while(tokIter.hasNext()){
            Annotation tAnn = (Annotation)tokIter.next();
            text += (String)tAnn.getFeatures().get("string");
            length += Integer.parseInt((String)tAnn.getFeatures().get("length"));
        }

        //create new annotations and features
        gate.FeatureMap features = Factory.newFeatureMap();
        features.put("kind", "word");
        features.put("string", text);
        features.put("length", length);
        features.put("orth", "upperInitial");
        outputAS.add(leftAS.firstNode(), leftAS.lastNode(), "Token", features);
    }
}
